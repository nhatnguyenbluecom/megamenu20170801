<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Menu;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory
     */
    protected $megaMenuFactory;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuReposity;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megamenuFactory
     * @param \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuReposity
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megamenuFactory,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuReposity
    ) {
        $this->megaMenuFactory = $megamenuFactory;
        $this->megaMenuReposity = $megaMenuReposity;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Isobar_Megamenu::megamenu')->addBreadcrumb(__('Mega Menu'), __('Mega Menu'));
        $resultPage->getConfig()->getTitle()->prepend(__('Mega Menu Management'));
        return $resultPage;
    }
}
