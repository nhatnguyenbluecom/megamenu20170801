<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\Megamenu\Model\Admin\Config\Source\Rootmenu;


class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;
    /**
     * Options constructor.
     * @param \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\SortOrder $sortOrder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder
     * @param \Magento\Framework\App\Request\Http $request
     */
    public function __construct(
        \Magento\Store\Model\StoreFactory $storeFactory
    ) {
        $this->_storeFactory = $storeFactory;
    }

    /**
     * @return array|null
     */
    public function toOptionArray()
    {
        return $stores = $this->_storeFactory->create()->getCollection()
            ->addStatusFilter(1)
            ->toOptionArray();
    }
}