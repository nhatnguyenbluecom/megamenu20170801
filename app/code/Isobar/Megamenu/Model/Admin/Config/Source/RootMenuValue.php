<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Isobar\Megamenu\Model\Admin\Config\Source;

use Magento\Framework\Data\ValueSourceInterface;

/**
 * Class RootMenuValue
 */
class RootMenuValue implements ValueSourceInterface
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * RootMenuValue constructor.
     * @param \Magento\Framework\App\Request\Http $request
     */
    public function __construct(\Magento\Framework\App\Request\Http $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue($name)
    {
        $rootId = $this->request->getParam('root_id');
        return $rootId;
    }
}
