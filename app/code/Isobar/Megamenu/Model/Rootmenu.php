<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\Megamenu\Model;


class Rootmenu extends \Magento\Framework\Model\AbstractModel implements \Isobar\Megamenu\Api\Data\RootmenuInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Megamenu\Model\ResourceModel\Rootmenu');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->_getData('id');
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreId()
    {
        return $this->_getData(self::STORE_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setStoreId($storeId)
    {
        return $this->setData('store_id', $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        return $this->setData('title', $title);
    }

    /**
     * {@inheritdoc}
     */
    public function getSort()
    {
        return $this->_getData(self::SORT);
    }

    /**
     * {@inheritdoc}
     */
    public function setSort($sort)
    {
        return $this->setData('sort', $sort);
    }

    /**
     * {@inheritdoc}
     */
    public function getAsRootMenu()
    {
        return $this->_getData(self::AS_ROOT_MENU);
    }

    /**
     * {@inheritdoc}
     */
    public function setAsRootMenu($asRootMenu)
    {
        return $this->setData('as_root_menu', $asRootMenu);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData('status', $status);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt() {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($timeStamp) {
        return $this->setData(self::CREATED_AT, $timeStamp);
    }
}