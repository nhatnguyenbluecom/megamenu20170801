<?php

namespace Isobar\Megamenu\Model\ResourceModel\Rootmenu;


class Collection extends \Isobar\Megamenu\Model\ResourceModel\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * _contruct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Megamenu\Model\Rootmenu', 'Isobar\Megamenu\Model\ResourceModel\Rootmenu');
    }
}