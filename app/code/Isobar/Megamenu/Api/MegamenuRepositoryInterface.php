<?php

namespace Isobar\Megamenu\Api;


interface MegamenuRepositoryInterface
{
    /**
     * @param \Isobar\Megamenu\Api\Data\MegamenuInterface $megamenu
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Isobar\Megamenu\Api\Data\MegamenuInterface $megamenu);

    /**
     * Get info about megamenu by id
     *
     * @param int $modelId
     * @return \Isobar\Megamenu\Api\Data\MegamenuInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($modelId);

    /**
     * Retrieve megamenu.
     *
     * @param int $id
     * @return \Isobar\Megamenu\Api\Data\MegamenuInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Delete megamenu
     *
     * @param \Isobar\Megamenu\Api\Data\MegamenuInterface $megamenu
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Isobar\Megamenu\Api\Data\MegamenuInterface $megaMenu);

    /**
     * Delete megamenu by id
     *
     * @param int $id
     * @return bool will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id);

    /**
     * Get megamenu list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Isobar\Megamenu\Api\Data\MegamenuSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}