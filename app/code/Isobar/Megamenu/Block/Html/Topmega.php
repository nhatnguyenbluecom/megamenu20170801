<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Isobar\Megamenu\Block\Html;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Theme\Block\Html\Topmenu;
use Magento\Cms\Model\BlockRepository;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\Registry;
/**
 * Html page top menu block
 */
class Topmega extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'Isobar_Megamenu::html/top_megamenu.phtml';

    /**
     * @var \Isobar\Megamenu\Api\RootmenuRepositoryInterface
     */
    protected $rootMenuRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Isobar\Megamenu\Helper\Data
     */
    protected $helper;

    /**
     * Topmega constructor.
     * @param Template\Context $context
     * @param \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuRepository
     * @param \Isobar\Megamenu\Model\Admin\Config\Source\Options $megaMenuOptions
     * @param \Isobar\Megamenu\Helper\Data $helper
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuRepository,
        \Isobar\Megamenu\Model\Admin\Config\Source\Options $megaMenuOptions,
        \Isobar\Megamenu\Helper\Data $helper,
        array $layoutProcessors = [],
        array $data = []
    )
    {
        $this->rootMenuRepository = $rootMenuRepository;
        $this->storeManager = $context->getStoreManager();
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Get mega menu html
     * @return string
     */
    public function getMegaMenuHtml()
    {
        $html = '';
        $filterGroups = [];
        $rootMenuId = $this->getRootMenuFromCurrentStore();
        if ($rootMenuId) {
            $items = $this->helper->getMenuItemsByRootId($rootMenuId);
            $treeData = $this->helper->getMenuItemTreeData($items);
            $html = $this->buildHierachy(0, $treeData);
        }
        return $html;
    }


    /**
     * Prepare options data
     * @param $parentId
     * @param $treeData
     * @param int $level
     */
    public function buildHierachy($parentId, $treeData, $level = -1)
    {
        $html = '';
        $level ++;
        if (isset($treeData['parents'][$parentId])) {
            foreach ($treeData['parents'][$parentId] as $itemId) {
                $style = $treeData['items'][$itemId]->getColorCode() ? 'style="color: '.$treeData['items'][$itemId]->getColorCode().'"' : '';
                $html .= '<li class="level-'. $level .'"><a '.$style.' href="' .$treeData['items'][$itemId]->getLink(). '">'. $treeData['items'][$itemId]->getTitle() .'</a>';
                if (in_array($itemId, $treeData['parents'][$parentId])) {
                    if ($recursiveHtml = $this->buildHierachy($itemId, $treeData, $level)) {
                        $class = '';
                        if ($level == 0) {
                            $html .= '<div>';
                            if (!$treeData['items'][$itemId]->getExtendHtml()) {
                                $class = "full-width";
                            }

                        }
                        $html .= '<ul class="'.$class.'">'. $this->buildHierachy($itemId, $treeData, $level) . '</ul>';
                        if ($level == 0 && $treeData['items'][$itemId]->getExtendHtml()) {
                            $html .= '<div class="menu-extend-html">'.$treeData['items'][$itemId]->getExtendHtml().'</div></div>';
                        }

                    }
                }
                $html .= '</li>';
            }
        }
        return $html;
    }

    public function getRootMenuFromCurrentStore()
    {
        $currentStoreId = $this->storeManager->getStore()->getId();
        $rootMenuId = $this->rootMenuRepository->getRootIdByStoreId($currentStoreId);
        return $rootMenuId;
    }
}
