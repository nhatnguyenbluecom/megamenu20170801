<?php
namespace Isobar\Megamenu\Observer;

use Magento\Framework\Event\ObserverInterface;

class UpdateHandle implements ObserverInterface
{
    /**
     * Https request
     *
     * @var \Zend\Http\Requestz
     */
    protected $_request;
    protected $_layout;
    protected $_data;
    protected $helperConfig;

    /**
     * @param Item $item
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Isobar\Megamenu\Helper\Config $helperConfig
    ) {
        $this->helperConfig = $helperConfig;
        $this->_layout = $context->getLayout();
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helperConfig->getStatus()) {
            return;
        }
        $this->_layout->getUpdate()->addHandle('default_megamenu');
    }
}
